import { Channel } from "amqplib";

//consumer service
interface Consumer<T> {
  subscribe: (handler: (msg: T) => void) => void;
}

//hide the rabbitmq api, so we are able to put anything under the code, we don't have to rewrite everything, only the factory functions
export const queueConsumerFactory = async <T>(channel: Channel, queue: string): Promise<Consumer<T>> => { 
  //queue létrehozása:
  await channel.assertQueue(queue);
  return {
    subscribe: (handler: (msg: T) => void) => {
      channel.consume(queue, (msg: any) => {
        if (msg !== null) {
          //a rabbitmq nem törődik a típussal, itt tudjuk megadni a típusát a kapott message-nek
          handler(JSON.parse(msg.content.toString()));
          //ack = aknowledgement, megkaptam az üzenetet, ha elosztva működik, akkor ez jelzi h megérkezett az üzenet, máshova nem érkezik meg
          channel.ack(msg);
        } else {
          console.log('Consumer cancelled by server');
        }
      });
    }
  }
}