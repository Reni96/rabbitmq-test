import amqplib from 'amqplib';
import { queuePublisherFactory } from './rabbitmq/publisher-factory';
import { queueConsumerFactory } from './rabbitmq/consumer-factory';

enum Queues {
  REQUEST_LIVE_AUCTION = 'live-auction-create-client-req',
  ANSWER_LIVE_AUCTION = 'live-auction-create-client-ans'
}

type TestMessage = {
  cica: string;
}

(async () => {
  
  const conn = await amqplib.connect('amqp://localhost');

  //channel = bármi, amin kommunikációt végzek, receive, consume, publish, send, 1 channel az összes publishra, és külön channelek a consumereknek
  const consumerChannel = await conn.createChannel();
  const senderChannel = await conn.createChannel();

  const publisher = await queuePublisherFactory<TestMessage>(senderChannel, Queues.ANSWER_LIVE_AUCTION);
  const consumer = await queueConsumerFactory<TestMessage>(consumerChannel, Queues.REQUEST_LIVE_AUCTION);

  consumer.subscribe( async (msg) => {
    console.log('Recieved:', msg.cica);
    await publisher.publish({...msg, cica: msg.cica+'kecske'});
  })
})();
