import { Channel } from "amqplib";

interface Publisher<T> {
  publish: (msg: T) => Promise<void>;
}
//2 külön channel a consume-hoz és a publish-hoz
export const queuePublisherFactory = async <T>(channel: Channel, queue: string): Promise<Publisher<T>> => { 
  //queue létrehozása
  await channel.assertQueue(queue);
  return {
    publish: async (message: T)=> {
      //üzenet küldés, Jsonné alakít, consume-nél visszaalakítjuk stringgé/amivé szeretnénk.
      await channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
    }
  };
};