import amqplib from 'amqplib';
import { queuePublisherFactory } from './rabbitmq/publisher-factory';
import { queueConsumerFactory } from './rabbitmq/consumer-factory';

enum Queues {
  REQUEST_ADMIN = 'admin-create-client-req',
  ANSWER_ADMIN = 'admin-create-client-ans',
  REQUEST_LIVE_AUCTION = 'live-auction-create-client-req',
  ANSWER_LIVE_AUCTION = 'live-auction-create-client-ans'
}

type TestMessage = {
  cica: string;
}

(async () => {
  const conn = await amqplib.connect('amqp://localhost');

  //channel = bármi, amin kommunikációt végzek, receive, consume, publish, send, 1 channel az összes publishra, és külön channelek a consumereknek
  const consumerChannel = await conn.createChannel();
  const senderChannel = await conn.createChannel();

  const adminPublisher = await queuePublisherFactory<TestMessage>(senderChannel, Queues.ANSWER_ADMIN);
  const adminConsumer = await queueConsumerFactory<TestMessage>(consumerChannel, Queues.REQUEST_ADMIN);

  const LAPublisher = await queuePublisherFactory<TestMessage>(senderChannel, Queues.REQUEST_LIVE_AUCTION);
  const LAConsumer = await queueConsumerFactory<TestMessage>(consumerChannel, Queues.ANSWER_LIVE_AUCTION);


  adminConsumer.subscribe( async (msg) => {
    console.log('Recieved:', msg.cica);
    await LAPublisher.publish(msg);
  })

  LAConsumer.subscribe( async (msg) => {
    console.log('Recieved:', msg.cica);
    await adminPublisher.publish(msg);
  })
})();
