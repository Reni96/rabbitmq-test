import Fastify from 'fastify';
import amqplib from 'amqplib';
import { queuePublisherFactory } from './rabbitmq/publisher-factory';
import { queueConsumerFactory } from './rabbitmq/consumer-factory';

enum Queues {
  REQUEST_ADMIN = 'admin-create-client-req',
  ANSWER_ADMIN = 'admin-create-client-ans',
}

type TestMessage = {
  cica: string;
}

(async () => {
  const conn = await amqplib.connect('amqp://localhost');

  //channel = bármi, amin kommunikációt végzek, receive, consume, publish, send, 1 channel az összes publishra, és külön channelek a consumereknek
  const consumerChannel = await conn.createChannel();
  const senderChannel = await conn.createChannel();

  const publisher = await queuePublisherFactory<TestMessage>(senderChannel, Queues.REQUEST_ADMIN);
  const consumer = await queueConsumerFactory<TestMessage>(consumerChannel, Queues.ANSWER_ADMIN);

  const consume = consumer.subscribe((msg) => console.log('Recieved:', msg.cica))
  console.log(consume)

  const fastify = Fastify({
    logger: true,
  });
  
  // Declare a route
  fastify.get('/', function (request, reply) {
    publisher.publish({cica: 'Meow!:3'})
    reply.send('Helllo there!');
  });
  
  // Run the server!
  fastify.listen({ port: 3000 }, function (err, address) {
    if (err) {
      fastify.log.error(err);
      process.exit(1);
    }
  });
})();
