# rabbitmq-test

## Getting started

- Start our docker container inside the /docker folder with the following code:
```
docker-compose up
```

- Start the project with the following code:
```
yarn start:watch
```
## Name
RabbitMQ Test

## Description
RabbitMQ queue service 

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Support
If you have any question about this wonderful mini-project, feel free to ask one of the following developers mentioned in the "Authors and acknowledgement" section.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
- Reni
- Ákos

## References
- https://miro.com/app/board/uXjVOrRqLxM=/?share_link_id=784743554988

